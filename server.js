const express = require('express')
const http = require('http');
const fs = require('fs');
const ExcelJS = require('exceljs');
const app = express()
const port = 5001

app.use(express.json());

app.get('/', (req, res) => {
  res.send('Hello World!')
})

const downloadExcel = async () => {
    const url = 'http://doc.contraloria.gob.pe/pas/documentos/Registro_sanciones_inscritas_vigentes_Actualizado_30NOV2020.xlsx'
    const splitUrl = url.split('/')
    const fileName = splitUrl[splitUrl.length - 1]
    await http.get(url, (response) => {
        if (response.statusCode === 200) {
            const file = fs.createWriteStream(fileName);
            response.pipe(file);
        }
    });
    return fileName
}

const readByDni = async (dni, excelFilePath) => {
    let results = dni.map((item) => ({
        'dni': item,
        'aparece': false
    }));
    const workbook = new ExcelJS.Workbook();
    await workbook.xlsx.readFile(excelFilePath)
    .then(() => {
        ws = workbook.worksheets[0]
        ws.eachRow((row) => {
            results.map((item, i) => {
                if (row.values[3] == item.dni) {
                    results[i].aparece = true
                }
            })
        });
        
    });
    
    return results
}

app.post('/consultar_persona', (req, res) => {
    downloadExcel().then((fileName) => {
        readByDni(req.body.dni, fileName).then ((results) => {
            res.status(200).json({
                estado: true,
                resultados: results
            })
        })
    });  
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})